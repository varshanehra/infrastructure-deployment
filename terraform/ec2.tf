module "private_ec2" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 4.0"

  # Autoscaling group
  name = "asg-private-instances"

  min_size                  = var.min_size
  max_size                  = var.max_size
  desired_capacity          = var.desired_capacity
  wait_for_capacity_timeout = 0
  health_check_type         = "EC2"
  vpc_zone_identifier       = module.vpc.private_subnets

  # Launch template
  lt_name                = "private-lt"
  user_data_base64       = data.template_cloudinit_config.private.rendered
  security_groups        = [module.private_sg.security_group_id]
  description            = "Launch template for EC2 instances in private subnets"
  update_default_version = true
  root_block_device = [{
    encrypted = true
  }]

  use_lt    = true
  create_lt = true

  image_id          = var.ami_image_id
  instance_type     = var.instance_type
  target_group_arns = module.alb.target_group_arns
  ebs_optimized     = true
  enable_monitoring = true

  block_device_mappings = [
    {
      # Root volume
      device_name = "/dev/xvda"
      no_device   = 0
      ebs = {
        delete_on_termination = true
        encrypted             = true
        volume_size           = 20
        volume_type           = "gp2"
      }
      }, {
      device_name = "/dev/sda1"
      no_device   = 1
      ebs = {
        delete_on_termination = true
        encrypted             = true
        volume_size           = 30
        volume_type           = "gp2"
      }
    }
  ]

  tags = [
    {
      key                 = "environment"
      value               = "infrastructure-deployment"
      propagate_at_launch = true
    },
    {
      key                 = "Category"
      value               = "private-instance"
      propagate_at_launch = true
    },
  ]
}



data "template_file" "private" {
  template = file("${path.module}/scripts/nginx_setup.tmpl")

  #vars = {
  #   add required variables here
  # }
}


data "template_cloudinit_config" "private" {
  gzip          = true
  base64_encode = true

  # Main cloud-config configuration file.
  part {
    filename     = "nginx.sh"
    content_type = "text/x-shellscript"
    content      = data.template_file.private.rendered
  }
}

