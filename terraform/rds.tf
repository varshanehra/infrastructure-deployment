module "db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 3.0"

  identifier                          = "postgres"
  engine                              = "postgres"
  engine_version                      = "13.3"
  instance_class                      = "db.t3.small"
  allocated_storage                   = 5
  name                                = "postgres"
  username                            = random_string.username.result
  password                            = random_password.password.result
  port                                = "5432"
  iam_database_authentication_enabled = true
  vpc_security_group_ids              = [module.rds_sg.security_group_id]
  maintenance_window                  = "Mon:00:00-Mon:03:00"
  backup_window                       = "03:00-06:00"
  monitoring_interval                 = "30"
  create_monitoring_role              = true
  monitoring_role_name                = var.monitoring_role_name

  # DB subnet group
  subnet_ids = module.vpc.private_subnets
  # DB parameter group
  family = "postgres13"
  # DB option group
  major_engine_version = "13.3"
  # Database Deletion Protection
  deletion_protection = true
}

resource "random_string" "username" {
  length  = 16
  special = false
}

resource "random_password" "password" {
  length           = 15
  special          = true
  override_special = "_!"
}

resource "aws_ssm_parameter" "password" {
  name        = "/dev/rds/password"
  description = "Storing password in SSM"
  type        = "SecureString"
  value       = random_password.password.result
  overwrite   = true
}

resource "aws_ssm_parameter" "username" {
  name        = "/dev/rds/username"
  description = "Storing username in SSM"
  type        = "SecureString"
  value       = random_string.username.result
  overwrite   = true
}
