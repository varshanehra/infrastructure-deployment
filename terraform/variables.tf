## Variables defines for VPC
variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}

variable "vpc_name" {
  description = "Name of the VPC"
  type        = string
  default     = "my-vpc"
}

variable "vpc_cidr" {
  description = "cidr-block for VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "availability_zone_names" {
  description = "Availability zones within the VPC"
  type        = list(string)
  default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "default_tag" {
  description = "default tags"
  type        = map(string)
  default = {
    environment = "infrastructure-deployment"
  }
}

variable "private_subnets" {
  description = "A list of cidr-block for private-subnets"
  type        = list(string)
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "public_subnets" {
  description = "A list of cidr-block for public-subnets"
  type        = list(string)
  default     = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
}

variable "enable_nat_gateway" {
  description = "Value should be true if NAT gateways to be enabled for instances in private subnets"
  type        = bool
  default     = true
}

variable "one_nat_gateway_per_az" {
  description = "Value should be true if only a NAT Gateway to be provisioned per availability zone."
  type        = bool
  default     = true
}

variable "single_nat_gateway" {
  description = "Value should be true if you want to provision a single shared NAT Gateway across all of your private networks"
  type        = bool
  default     = false
}

variable "enable_dns_hostnames" {
  description = "Value should be true to enable DNS hostnames in the VPC"
  type        = bool
  default     = true
}



## Variables defines for Ec2
variable "instance_type" {
  description = "Ec2 instance type"
  type        = string
  default     = "t3.nano"
}

variable "ami_image_id" {
  description = "Ec2 ami-image id"
  type        = string
  default     = "ami-0ed9277fb7eb570c9"
}

variable "min_size" {
  description = "Minimum size for ec2 instances"
  type        = number
  default     = 1
}

variable "max_size" {
  description = "Maximum size for ec2 instances"
  type        = number
  default     = 3
}

variable "desired_capacity" {
  description = "Desired number of ec2-instances  available all time"
  type        = number
  default     = 2
}


## Variables defines for RDS
variable "monitoring_role_name" {
  description = "Monitoring role name"
  type        = string
  default     = "mypostgresmonitoringrole"
}



