output "load_balancer_dns_name" {
  description = "ALB DNS connected to private EC2"
  value       = module.alb.lb_dns_name
}

output "db_instance_address" {
  description = "The address of the RDS instance"
  value       = module.db.db_instance_address
}


