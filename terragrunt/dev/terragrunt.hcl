include {
  path = find_in_parent_folders()
}

terraform {
  source = "${get_parent_terragrunt_dir()}/..//terraform"
}

# Input values

inputs = {
  cluster_name = "infrastructure-deployment"  
}
