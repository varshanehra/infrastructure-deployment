remote_state {
  backend = "s3"
  generate = {
    path      = "s3backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket         = get_env("TFSTATE_BUCKET", "infrastructure-deployment-2021")
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = local.region
    encrypt        = true
    dynamodb_table = "lock-table"
  }
}

locals {
  region = "us-east-1"
}

inputs = {
  region = local.region
  default_tags = {
    environment = "infrastructure-deployment"
  }
  region = "us-east-1"
  vpc_name = "infra"
  vpc_cidr = "10.0.0.0/16"
  availability_zone_names = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  enable_nat_gateway = true
  one_nat_gateway_per_az = true
  single_nat_gateway = false
  enable_dns_hostnames = true
  instance_type = "t3.nano"
  ami_image_id = "ami-0ed9277fb7eb570c9"
  min_size = 1
  max_size = 3
  desired_capacity = 2
  monitoring_role_name = "mypostgresmonitoringrole"
}
