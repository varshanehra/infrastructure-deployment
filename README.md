# Option 2: Infrastructure Deployment
Using terraform, automate the build of the following application in AWS. For the purposes of this challenge, use any Linux based AMI id for the two EC2 instances and simply show how they would be provisioned with the connection details for the RDS.

![image](image1.png "Infrastructure")

There are a lot of additional resources to create even in this simple setup, you can use the code we have made available that will create some structure and deploy the networking environment to make it possible to plan/apply the full deployment. Feel free to use and/or modify as much or as little of it as you like.
Document any assumptions or additions you make in the README for the code repository. You may also want to consider and make some notes on:



## Notes:
- I have used terraform community modules to quickly deploy the resources.
1. How would a future application obtain the load balancer’s DNS name if it wanted to use this service?
- We will create CNAME/Alias record for load balancer’s DNS name in Route53. This will not only be helpful for future application but also it will keep load balancer DNS static even after it's recreation.
2. What aspects need to be considered to make the code work in a CD pipeline (how does it successfully and safely get into production)?
- We should add some testing jobs like tfsec for security test and secret-scanning.
- Update load-balancer to listen on port 443 (https) and redirect all request on port 443 from port 80.
- We should enable AWS WAF and AWS Shield Advanced for the load-balancer or even use Cloudfront-distribution which can additionally respond with custom-error pages.
- We should create RDS into multi-AZ for high-availability and read-replicas for better efficiency. Timely RDS backup should also be provisioned. 
- Setup monitoring and alerting for Ec2 instances and RDS
- Make use of IAM roles instead of using AWS credentials (access key and secret-access-key).
- Update instance-type for Ec2 and RDS as per the requirements.





